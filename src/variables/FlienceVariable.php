<?php
/**
 * flience plugin for Craft CMS 3.x
 *
 * Flience's limits
 *
 * @link      https://github.com/pleodigital/
 * @copyright Copyright (c) 2019 pleodigital
 */

namespace pleodigital\flience\variables;

use pleodigital\flience\Flience;

use Craft;

/**
 * flience Variable
 *
 * Craft allows plugins to provide their own template variables, accessible from
 * the {{ craft }} global variable (e.g. {{ craft.flience }}).
 *
 * https://craftcms.com/docs/plugins/variables
 *
 * @author    pleodigital
 * @package   Flience
 * @since     1.0.0
 */
class FlienceVariable
{
    // Public Methods
    // =========================================================================

    /**
     * Whatever you want to output to a Twig template can go into a Variable method.
     * You can have as many variable functions as you want.  From any Twig template,
     * call it like this:
     *
     *     {{ craft.flience.exampleVariable }}
     *
     * Or, if your variable requires parameters from Twig:
     *
     *     {{ craft.flience.exampleVariable(twigValue) }}
     *
     * @param null $optional
     * @return string
     */
    public function settings()
    {
        return craft()->plugins->getPlugin('flience')->getSettings();
    }
}
