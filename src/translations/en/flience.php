<?php
/**
 * flience plugin for Craft CMS 3.x
 *
 * Flience's limits
 *
 * @link      https://github.com/pleodigital/
 * @copyright Copyright (c) 2019 pleodigital
 */

/**
 * flience en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('flience', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    pleodigital
 * @package   Flience
 * @since     1.0.0
 */
return [
    'flience plugin loaded' => 'flience plugin loaded',
];
