<?php

namespace pleodigital\flience\models;
use craft\base\Model;
use pleodigital\flience\Flience;

class Settings extends Model
{
    public $limit = 1;

    public function rules()
    {
        return [
            ['limit', 'number'],
        ];
    }
}
