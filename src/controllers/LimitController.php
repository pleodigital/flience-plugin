<?php
/**
 * flience plugin for Craft CMS 3.x
 *
 * Flience's limits
 *
 * @link      https://github.com/pleodigital/
 * @copyright Copyright (c) 2019 pleodigital
 */

namespace pleodigital\flience\controllers;

use craft\db\Connection;
use craft\db\Query;
use craft\web\twig\variables\UserSession;
use pleodigital\flience\Flience;

use Craft;
use craft\web\Controller;

/**
 * Default Controller
 *
 * Generally speaking, controllers are the middlemen between the front end of
 * the CP/website and your plugin’s services. They contain action methods which
 * handle individual tasks.
 *
 * A common pattern used throughout Craft involves a controller action gathering
 * post data, saving it on a model, passing the model off to a service, and then
 * responding to the request appropriately depending on the service method’s response.
 *
 * Action methods begin with the prefix “action”, followed by a description of what
 * the method does (for example, actionSaveIngredient()).
 *
 * https://craftcms.com/docs/plugins/controllers
 *
 * @author    pleodigital
 * @package   Flience
 * @since     1.0.0
 */
class LimitController extends Controller
{
    // Protected Properties
    // =========================================================================

    /**
     * @var    bool|array Allows anonymous access to this controller's actions.
     *         The actions must be in 'kebab-case'
     * @access protected
     */
    protected $allowAnonymous = ['index', 'do-something', 'get-limit', 'is-logged-in', 'max-limit', 'get-account-type', 'upgrade-account'];

    // Public Methods
    // =========================================================================

    public function actionAddDownload() {
        $db = new Connection([
            "dsn" => getenv('DB_DRIVER').":host=".getenv('DB_SERVER').";dbname=".getenv('DB_DATABASE'),
            "username" => getenv('DB_USER'),
            "password" => getenv('DB_PASSWORD'),
            "charset" => "utf8"
        ]);

        for ($i = 0; $i < $_POST["amountOfWallpapers"]; $i++) {
            $db->createCommand()->insert("userslimit", [
                "downloaded" => Craft::$app->getUser()->getId(),
                "dateCreated" => date("Y-m-d H:i:s")
            ])->execute();
        }

        $query = new Query;

        // $select = count($db->createCommand("SELECT * FROM userslimit WHERE id='".Craft::$app->getUser()->getId()."'"));
        $select = $query->select("*")->from("userslimit")->where("downloaded=".Craft::$app->getUser()->getId())->andWhere("dateCreated>CURDATE() - INTERVAL 1 MONTH")->count();

        $limit = Flience::getInstance()->settings->limit;

        if ($select > $limit) {
            return 0;
        } else {
            return $select;
        }

        // return Craft::$app->getUser()->id;
        // return CJSON::encode($select);
        // return $select;
        // return 1;
    }

    public function actionGetLimit() {
        if (Craft::$app->getUser()->getId()) {

            $query = new Query();

            $select = $query->select("*")->from("userslimit")->where("downloaded=".Craft::$app->getUser()->getId())->andWhere("dateCreated>CURDATE() - INTERVAL 1 MONTH")->count();

            $limit = Flience::getInstance()->settings->limit;

            if ($this->actionGetAccountType()) {
                return $limit - 1;
            } else {
                if ($select <= $limit) {
                    return $limit - $select;
                } else {
                    return $select;
                }
            }
        } else {
            return 'error';
        }
    }

    public function actionIsLoggedIn() {
        if ((Craft::$app->getUser()->getId()) !== null) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Handle a request going to our plugin's index action URL,
     * e.g.: actions/users-limit/default
     *
     * @return mixed
     */
    public function actionMaxLimit() {

        if (Craft::$app->getUser()->getId()) {
            return Flience::getInstance()->settings->limit;
        } else {
            return 'error';
        }
    }

    public function actionGeneratePdf( array $ids, $type) {
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 0,
            'margin_right' => 0,
            'margin_top' => 0,
            'margin_bottom' => 0,
            'margin_header' => 0,
            'margin_footer' => 0
        ]);
        // TODO: dodawanie pobranych fiszek do bazy przenosimy tutaj, niebezpiecznie jest dodawać to po stronie frontu
        $userId = Craft :: $app -> getUser() -> getId();
        $request = Craft :: $app -> getRequest();

        $fronts = [];
        $backs = [];

        foreach ($ids as $id) {
            $fronts[] = $id['front'];
            $backs[] = $id['back'];
        }

        $itemsOnPage = $type === 'math' ? 4 : 8;

        $frontChunks = [];

        foreach ($fronts as $index=>$front) {
            $chunkIndex = floor($index/$itemsOnPage);

            if (!isset($frontChunks[$chunkIndex])) {
                $frontChunks[$chunkIndex] = [];
            }

            array_push($frontChunks[$chunkIndex], $front);
        }

        $backChunks = [];

        foreach ($backs as $index=>$backs) {
            $chunkIndex = floor($index/$itemsOnPage);

            if (!isset($backChunks[$chunkIndex])) {
                $backChunks[$chunkIndex] = [];
            }

            array_push($backChunks[$chunkIndex], $backs);
        }

        $pages = count($frontChunks) * 2;

        $cardsArray = [];

        $index = 0;

        if ( $type === 'lang') {
            for ($i = 0; $i < $pages; $i++) {
                if ($i%2 === 0) {
                    array_push($cardsArray, $frontChunks[$index]);
                } else {
                    array_push($cardsArray, $backChunks[$index]);
                    $index++;
                }
            }
        } else {
            for ($i = 0; $i < $pages; $i++) {
                if ($i%2 === 0) {
                    array_push($cardsArray, $backChunks[$index]);
                } else {
                    array_push($cardsArray, $frontChunks[$index]);
                    $index++;
                }
            }
        }


        $html = '';
        $stylesheet = '<style media="print">
            * {
            margin: 0px 1px !important;
            padding: 0 !important;
            box-sizing: border-box;
            }
            
            .container {
            width: 800px;
            }
            
            pre {
                width: 396px !important;
                display: inline-block;
                margin: 0;
            }
            
            pre.front {
                float: left !important;
            }
            
            pre.back {
                float: right !important;
            }
        </style>';
        $mpdf -> WriteHTML($stylesheet, 1);
        echo '<br>';

        $mpdf->WriteHTML('<div class="container">');

        foreach ($cardsArray as $index=>$cardPage) {
            if ($index%2 === 0) {
//                echo 'FRONTTTT<br>';
                foreach ($cardPage as $item) {
                    $html .= $this->getImage($item, 'front');
//                    echo 'front - '.$item.'<br>';
                }
            } else {
//                echo 'BACKKK<br>';
                foreach ($cardPage as $item) {
                    $html .= $this->getImage($item, 'back');
//                    echo 'back - '.$item.'<br>';
                }
            }
//            echo $html;
            $mpdf->WriteHTML($html);

            if (count($cardsArray) !== $index + 1) {
                $html = '';
//                echo '<br>NOWA<br>';
                $mpdf->AddPage();
            }

        }

        $mpdf->WriteHTML('</div>');

        $mpdf -> Output();
//        return 1;
    }

    private function getImage($id, $side) {
        $asset = \craft\elements\Asset::find()
            -> id($id)
            -> one();

        return '<pre class="'.$side.'"><img src="'.$asset->getUrl().'" ></pre>';
    }

    public function actionGetAccountType() {
        $user = Craft::$app->getUser();
        return $user->getIdentity()->getFieldValue('isPremium') ? 1 : 0;
    }

    /**
     * Handle a request going to our plugin's actionDoSomething URL,
     * e.g.: actions/users-limit/default/do-something
     *
     * @return mixed
     */
    public function actionDoSomething()
    {
        return Flience::getInstance()->getSettings()->limit;
    }
}
